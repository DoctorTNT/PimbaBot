# PimbaBot PYTHON 3.6+

<n1><b>PIP INSTALL</b></n1><br />
bash install.sh

<n1><b>INSTALL PYTHON3</b></n1><br />
<b>Android - Termux</b><br />
<a href="https://play.google.com/store/apps/details?id=com.termux&hl=pt_BR">Download Termux</a><br />
  pkg install python3
  
<b>Desktop - GNU/Linux</b><br />
  apt-get install python3.6
  
<b>Desktop - Windows</b><br />
  <a href="https://www.python.org/downloads/">Download Python3.6</a><br />
  
<n1><b>CONFIG</b></n1><br />
Entre em config.py<br />
1 - token bot<br />
2 - token shodan.io

<n1><b>START BOT</b></n1><br />
python3 PimbaBot.py

<n1><b>Telegram: @MrCooller or @Cooller0</b></n1><br />
